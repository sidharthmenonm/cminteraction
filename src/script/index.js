import axios from 'axios';

var button = document.getElementById('cal-button');
var modal = document.getElementById('modal-form');
var loader = document.getElementById('loader');

var submit_button = document.getElementById('submit-form');

button.addEventListener('click', function() {
  modal.style.display = 'block';
});

submit_button.addEventListener('submit', function(e) {
  e.preventDefault();
  var name = document.getElementById('stacked-name').value;
  var email = document.getElementById('stacked-email').value;
  var phone = document.getElementById('stacked-phone').value;
  var org = document.getElementById('stacked-org').value;

  var data = {
    name: name,
    email: email,
    phone: phone,
    organisation: org
  }
  var url = 'https://script.google.com/macros/s/AKfycbysUcOXtl_EJ2v20vtJCRhR5oV2SHlxNyCuKNnfejcVU3s0D0JAQOWf/exec'
  var xhr = new XMLHttpRequest();
  xhr.open('POST', url);
  // xhr.withCredentials = true;
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.onreadystatechange = function() {
    if (xhr.readyState === 4 && xhr.status === 200) {
      //success

      window.open('https://evt.mx/YpGPJqrm');
      loader.style.display = 'none';

    }
  };
  // url encode form data for sending as post data
  var encoded = Object.keys(data).map(function(k) {
    return encodeURIComponent(k) + "=" + encodeURIComponent(data[k]);
  }).join('&');

  xhr.send(encoded);

  modal.style.display = 'none';
  loader.style.display = 'block';

  console.log(name);
});

! function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (!d.getElementById(id)) {
    js = d.createElement(s);
    js.id = id;
    js.src = "https://plugins.eventable.com/eventable.js";
    fjs.parentNode.insertBefore(js, fjs);
  }
}(document, "script", "eventable-script");